package ru.shumov.tm.enums;

public enum Role {

    ADMINISTRATOR("Администратор"),
    USER("Обычный Пользователь");

    private String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
