package ru.shumov.tm.entity;

import lombok.Getter;
import lombok.Setter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
public class Task {
    DateFormat dateFormat = new SimpleDateFormat("dd.MM.YYYY");
    private String id;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private String projectId;
    private String userId;

    @Override
    public String toString() {
        return "Task{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + dateFormat.format(startDate) +
                ", endDate=" + dateFormat.format(endDate) +
                ", projectId='" + projectId + '\'' +
                '}';
    }
}
