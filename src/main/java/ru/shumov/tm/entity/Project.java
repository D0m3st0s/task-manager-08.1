package ru.shumov.tm.entity;

import lombok.Getter;
import lombok.Setter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
public class Project {
    DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private String id;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private String UserId;

    @Override
    public String toString() {
        return "Project{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + dateFormat.format(startDate) +
                ", endDate=" + dateFormat.format(endDate) +
                '}';
    }
}
