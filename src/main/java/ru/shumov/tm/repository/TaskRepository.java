package ru.shumov.tm.repository;

import ru.shumov.tm.entity.Task;

import java.util.Collection;

public interface TaskRepository {

    Collection<Task> findAll();

    Task findOne(String id);

    void persist(Task task);

    void merge(Task task);

    void remove(String id);

    void removeAll();
}
