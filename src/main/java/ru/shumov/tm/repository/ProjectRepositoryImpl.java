package ru.shumov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Project;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ProjectRepositoryImpl implements ProjectRepository {

    private Map<String, Project> projects = new HashMap<>();

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findOne(@NotNull String id) {
        return projects.get(id);
    }

    public void persist(@NotNull String id,@NotNull Project project) {
        if(!projects.containsKey(id)){
            projects.put(id, project);
        }
    }

    public void merge(@NotNull Project project) {
        projects.put(project.getId(), project);
    }

    public void remove(@NotNull String id) {
        projects.remove(id);
    }

    public void removeAll() {
        projects.clear();
    }
}
