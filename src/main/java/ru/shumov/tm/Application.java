package ru.shumov.tm;

import java.security.NoSuchAlgorithmException;

public class Application {
    public static void main(String[] args) throws NoSuchAlgorithmException {
        Bootstrap bootstrapImpl = new Bootstrap();
        bootstrapImpl.init();
        bootstrapImpl.start();
    }
}
