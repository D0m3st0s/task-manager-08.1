package ru.shumov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.exceptions.IncorrectInput;
import ru.shumov.tm.repository.TaskRepository;

import java.util.Collection;

public class TaskServiceImpl implements TaskService {
    private TerminalService data;
    private TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository, TerminalService data) {
        this.taskRepository = taskRepository;
        this.data = data;
    }

    public void create(@NotNull Task task) {
        try {
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new IncorrectInput(Constants.EXCEPTION_TASK + "task name");
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new IncorrectInput(Constants.EXCEPTION_TASK + "project id");
            }
            taskRepository.persist(task);
        }
        catch (IncorrectInput incorrectInput) {
            data.outPutString(incorrectInput.getMassage());
        }
    }

    public void clear() {
        taskRepository.removeAll();
    }

    public void remove(@NotNull String taskId) {
        taskRepository.remove(taskId);
    }

    public Collection<Task> getList() {
        Collection<Task> value = taskRepository.findAll();
        return value;
    }

    public Task getOne(@NotNull String id) {
        return taskRepository.findOne(id);
    }

    public void update(@NotNull Task task) {
        try {
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new IncorrectInput(Constants.EXCEPTION_TASK + "task name");
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new IncorrectInput(Constants.EXCEPTION_TASK + "project id");
            }
            taskRepository.merge(task);
        }
        catch (IncorrectInput incorrectInput) {
            data.outPutString(incorrectInput.getMassage());
        }
    }

    public boolean checkKey(@NotNull String id) {
        if(taskRepository.findOne(id) != null) {
            return taskRepository.findOne(id).getId().equals(id);
        }
        return false;
    }
}
