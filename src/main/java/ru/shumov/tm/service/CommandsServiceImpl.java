package ru.shumov.tm.service;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.command.*;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class CommandsServiceImpl implements CommandsService{
    private Bootstrap bootstrap;
    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public CommandsServiceImpl(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void commandsInit() {
        ProjectCreateCommand projectCreateCommand = new ProjectCreateCommand(bootstrap);
        commands.put(projectCreateCommand.getName(), projectCreateCommand);
        ProjectClearCommand projectClearCommand = new ProjectClearCommand(bootstrap);
        commands.put(projectClearCommand.getName(), projectClearCommand);
        ProjectRemoveCommand projectRemoveCommand = new ProjectRemoveCommand(bootstrap);
        commands.put(projectRemoveCommand.getName(), projectRemoveCommand);
        ProjectGetListCommand projectGetListCommand = new ProjectGetListCommand(bootstrap);
        commands.put(projectGetListCommand.getName(), projectGetListCommand);
        ProjectGetOneCommand projectGetOneCommand = new ProjectGetOneCommand(bootstrap);
        commands.put(projectGetOneCommand.getName(), projectGetOneCommand);
        ProjectUpdateCommand projectUpdateCommand = new ProjectUpdateCommand(bootstrap);
        commands.put(projectUpdateCommand.getName(), projectUpdateCommand);
        TaskCreateCommand taskCreateCommand = new TaskCreateCommand(bootstrap);
        commands.put(taskCreateCommand.getName(), taskCreateCommand);
        TaskClearCommand taskClearCommand = new TaskClearCommand(bootstrap);
        commands.put(taskClearCommand.getName(), taskClearCommand);
        TaskRemoveCommand taskRemoveCommand = new TaskRemoveCommand(bootstrap);
        commands.put(taskRemoveCommand.getName(), taskRemoveCommand);
        TaskGetListCommand taskGetListCommand = new TaskGetListCommand(bootstrap);
        commands.put(taskGetListCommand.getName(), taskGetListCommand);
        TaskGetOneCommand taskGetOneCommand = new TaskGetOneCommand(bootstrap);
        commands.put(taskGetOneCommand.getName(), taskGetOneCommand);
        TaskUpdateCommand taskUpdateCommand = new TaskUpdateCommand(bootstrap);
        commands.put(taskUpdateCommand.getName(), taskUpdateCommand);
        UserAuthorizationCommand userAuthorizationCommand = new UserAuthorizationCommand(bootstrap);
        commands.put(userAuthorizationCommand.getName(), userAuthorizationCommand);
        UserRegistrationCommand userRegistrationCommand = new UserRegistrationCommand(bootstrap);
        commands.put(userRegistrationCommand.getName(), userRegistrationCommand);
        UserLogOutCommand userLogOutCommand = new UserLogOutCommand(bootstrap);
        commands.put(userLogOutCommand.getName(), userLogOutCommand);
        UserPasswordUpdateCommand userPasswordUpdateCommand = new UserPasswordUpdateCommand(bootstrap);
        commands.put(userPasswordUpdateCommand.getName(), userPasswordUpdateCommand);
        UserShowCommand userShowCommand = new UserShowCommand(bootstrap);
        commands.put(userShowCommand.getName(), userShowCommand);
        AboutCommand aboutCommand = new AboutCommand();
        commands.put(aboutCommand.getName(), aboutCommand);
        Help help = new Help(this, bootstrap);
        commands.put(help.getName(), help);
        Exit exit = new Exit(bootstrap);
        commands.put(exit.getName(), exit);
    }

    public Collection<AbstractCommand> getList() {
        return commands.values();
    }

    public boolean checkKey(final String key) {
        if(commands.containsKey(key)){
            return true;
        } else {
            return false;
        }
    }

    public void commandExecute(final String key) throws NoSuchAlgorithmException {
        commands.get(key).execute();
    }
}
