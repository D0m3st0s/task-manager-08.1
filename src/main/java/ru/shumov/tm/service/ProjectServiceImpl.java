package ru.shumov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.exceptions.IncorrectInput;
import ru.shumov.tm.repository.ProjectRepository;

import java.util.Collection;

public class ProjectServiceImpl implements ProjectService {
    private ProjectRepository projectRepository;
    private TaskService taskService;
    private TerminalService data;

    public ProjectServiceImpl(ProjectRepository projectRepository, TerminalService data, TaskService taskService) {
        this.projectRepository = projectRepository;
        this.data = data;
        this.taskService = taskService;
    }

    public void create(@NotNull Project project) {
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInput(Constants.EXCEPTION_PROJECT + "project name");
            }
            projectRepository.persist(project.getId(), project);
        }
        catch (IncorrectInput incorrectInput) {
            data.outPutString(incorrectInput.getMassage());
        }
    }

    public void clear() {
        projectRepository.removeAll();
        taskService.clear();
    }

    public void remove(@NotNull String projectId) {
        try {
            if (projectId == null || projectId.isEmpty()) {
                throw new IncorrectInput(Constants.EXCEPTION_PROJECT + "project id");
            }
            projectRepository.remove(projectId);
        }
        catch (IncorrectInput incorrectInput) {
            data.outPutString(incorrectInput.getMassage());
        }
    }

    public Collection<Project> getList() {
        Collection<Project> values = projectRepository.findAll();
        return values;
    }

    public Project getOne(@NotNull String id) {
        return projectRepository.findOne(id);
    }

    public void update(@NotNull Project project) {
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInput(Constants.EXCEPTION_PROJECT + "project name");
            }
            projectRepository.merge(project);
        }
        catch (IncorrectInput incorrectInput) {
            data.outPutString(incorrectInput.getMassage());
        }
    }

    public boolean checkKey(String id) {
        if(projectRepository.findOne(id) != null) {
            return projectRepository.findOne(id).getId().equals(id);
        }
        return false;
    }
}
