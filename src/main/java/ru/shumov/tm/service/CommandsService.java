package ru.shumov.tm.service;

import ru.shumov.tm.command.AbstractCommand;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;

public interface CommandsService {

    void commandsInit();

    Collection<AbstractCommand> getList();

    boolean checkKey(final String key);

    void commandExecute(final String key) throws NoSuchAlgorithmException;
}
