package ru.shumov.tm;

import lombok.Getter;
import lombok.Setter;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.repository.*;
import ru.shumov.tm.service.*;
import ru.shumov.tm.service.TerminalService;
import ru.shumov.tm.service.Md5Service;

import java.security.NoSuchAlgorithmException;

import static ru.shumov.tm.Constants.*;

public class Bootstrap implements ServiceLocator, ServiceLocatorExit {

    private ProjectRepository projectRepository;
    @Getter private ProjectService projectService;
    private TaskRepository taskRepository;
    @Getter private TaskService taskService;
    private UserRepository userRepository;
    @Getter private UserService userService;
    @Getter private TerminalService terminalService;
    private CommandsService commandsService;
    @Getter private Md5Service md5Service = new Md5Service();
    @Getter @Setter
    private User user = null;
    private boolean work = true;

    public void setWork(boolean work) {
        this.work = work;
    }

    public void init() throws NoSuchAlgorithmException {
        terminalService = new TerminalService();
        projectRepository = new ProjectRepositoryImpl();
        projectService = new ProjectServiceImpl(projectRepository, terminalService, taskService);
        taskRepository = new TaskRepositoryImpl();
        taskService = new TaskServiceImpl(taskRepository, terminalService);
        userRepository = new UserRepositoryImpl();
        userService = new UserServiceImpl(userRepository);
        commandsService = new CommandsServiceImpl(this);
        userRepository.usersCreate();
        commandsService.commandsInit();
    }

    public void start() {
        terminalService.outPutString(WELCOME);
        terminalService.outPutString(PROJECT_ID);
        while (work) {
            try {
                String commandName = terminalService.scanner();
                if(commandsService.checkKey(commandName)) {
                    commandsService.commandExecute(commandName);
                } else {
                    terminalService.outPutString(COMMAND_DOES_NOT_EXIST);
                }
            }
            catch (NullPointerException | NoSuchAlgorithmException exception) {
                exception.printStackTrace();
            }
        }
    }
}
