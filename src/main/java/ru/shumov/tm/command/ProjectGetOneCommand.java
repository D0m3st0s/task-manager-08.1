package ru.shumov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;

public class ProjectGetOneCommand extends AbstractCommand{
    private ServiceLocator bootstrap;
    private final Role role = Role.USER;
    private final String name = "get project";
    private final String description = "get project: Вывод конкретного проекта.";

    public ProjectGetOneCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        @Nullable User user = bootstrap.getUser();
        if(user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ENTER_PROJECT_ID);
            @NotNull final var projectId = bootstrap.getTerminalService().scanner();
            @NotNull final var project = bootstrap.getProjectService().getOne(projectId);
            if (project.getUserId().equals(user.getId())) {
                bootstrap.getTerminalService().outPutProject(project);
            } else {
                bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
