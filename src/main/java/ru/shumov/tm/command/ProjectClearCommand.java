package ru.shumov.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.service.TerminalService;

public class ProjectClearCommand extends AbstractCommand{
    private ServiceLocator bootstrap;
    private final Role role = Role.ADMINISTRATOR;
    private final String name = "project clear";
    private final String description = "project clear: Удаление всех проектов.";

    public ProjectClearCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        User user = bootstrap.getUser();
        if(user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ALL_PROJECTS_WILL_BE_CLEARED);
            bootstrap.getTerminalService().outPutString(Constants.YES_NO);
            @NotNull final var answer = bootstrap.getTerminalService().scanner();
            if (answer.equals(Constants.YES)) {
                bootstrap.getProjectService().clear();
                bootstrap.getTaskService().clear();
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }



    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
