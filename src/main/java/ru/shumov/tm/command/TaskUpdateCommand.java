package ru.shumov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.service.TerminalService;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TaskUpdateCommand extends AbstractCommand{
    private ServiceLocator bootstrap;
    private final Role role = Role.USER;
    private final String name = "task update";
    private final String description = "task update: Изменения параметров задачи.";

    public TaskUpdateCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        var task = new Task();
        if(user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            var format = new SimpleDateFormat();
            format.applyPattern("dd.MM.yyyy");
            bootstrap.getTerminalService().outPutString(Constants.ENTER_ID_OF_TASK_FOR_SHOWING);
            @NotNull final var taskId = bootstrap.getTerminalService().scanner();
            if(!bootstrap.getTaskService().checkKey(taskId)) {
                bootstrap.getTerminalService().outPutString(Constants.PROJECT_DOES_NOT_EXIST);
                return;
            }
            if(bootstrap.getTaskService().checkKey(taskId)) {
                task = bootstrap.getTaskService().getOne(taskId);
            }
            if(!task.getUserId().equals(user.getId())) {
                bootstrap.getTerminalService().outPutString("У вас нет доступа к этой задаче.");
                return;
            }
            try {
                bootstrap.getTerminalService().outPutString(Constants.ENTER_TASK_NAME);
                @NotNull final var name = bootstrap.getTerminalService().scanner();
                bootstrap.getTerminalService().outPutString(Constants.ENTER_DESCRIPTION_OF_TASK);
                @NotNull final var description = bootstrap.getTerminalService().scanner();
                bootstrap.getTerminalService().outPutString(Constants.ENTER_START_DATE_OF_TASK);
                @NotNull final var startDate = bootstrap.getTerminalService().scanner();
                bootstrap.getTerminalService().outPutString(Constants.ENTER_DEADLINE_OF_PROJECT);
                @NotNull final var endDate = bootstrap.getTerminalService().scanner();
                task.setName(name);
                task.setDescription(description);
                task.setStartDate(format.parse(startDate));
                task.setEndDate(format.parse(endDate));
                bootstrap.getTaskService().update(task);
            } catch (ParseException parseException) {
                bootstrap.getTerminalService().outPutString(Constants.INCORRECT_DATE_FORMAT);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
