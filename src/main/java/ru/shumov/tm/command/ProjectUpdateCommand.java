package ru.shumov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.service.TerminalService;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ProjectUpdateCommand extends AbstractCommand{
    private ServiceLocator bootstrap;
    private final Role role = Role.USER;
    private final String name = "project update";
    private final String description = "project update: Изменение параметров проекта.";

    public ProjectUpdateCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        @Nullable var user = bootstrap.getUser();
        var project = new Project();
        if(user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            var format = new SimpleDateFormat();
            format.applyPattern("dd.MM.yyyy");
            bootstrap.getTerminalService().outPutString(Constants.ENTER_ID_OF_PROJECT_FOR_SHOWING);
            @NotNull final var projectId = bootstrap.getTerminalService().scanner();
            if(!bootstrap.getProjectService().checkKey(projectId)) {
                bootstrap.getTerminalService().outPutString(Constants.PROJECT_DOES_NOT_EXIST);
                return;
            }
            if(bootstrap.getProjectService().checkKey(projectId)) {
                project = bootstrap.getProjectService().getOne(projectId);
            }
            if(!project.getUserId().equals(user.getId())) {
                bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS_FOR_PROJECT);
                return;
            }
            try {
                System.out.println();
                @NotNull final var name = bootstrap.getTerminalService().scanner();
                System.out.println();
                @NotNull final var description = bootstrap.getTerminalService().scanner();
                System.out.println();
                @NotNull final var startDate = bootstrap.getTerminalService().scanner();
                System.out.println();
                @NotNull final var endDate = bootstrap.getTerminalService().scanner();
                project.setName(name);
                project.setDescription(description);
                project.setStartDate(format.parse(startDate));
                project.setEndDate(format.parse(endDate));
                bootstrap.getProjectService().update(project);
            } catch (ParseException parseException) {
                bootstrap.getTerminalService().outPutString(Constants.INCORRECT_DATE_FORMAT);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
