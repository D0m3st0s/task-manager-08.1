package ru.shumov.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.service.TerminalService;

import java.util.Collection;

public class TaskGetListCommand extends AbstractCommand{
    private ServiceLocator bootstrap;
    private final Role role = Role.USER;
    private final String name = "task list";
    private final String description = "task list: Вывод всех задач.";

    public TaskGetListCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        var counter = 0;
        @Nullable final var user = bootstrap.getUser();
        if(user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            @Nullable Collection<Task> values = bootstrap.getTaskService().getList();
            if (values.isEmpty()) {
                bootstrap.getTerminalService().outPutString(Constants.TASKS_DO_NOT_EXIST);
                return;
            }
            for(Task task: values) {
                if (task.getUserId().equals(bootstrap.getUser().getId())) {
                    counter++;
                }
            }
            if(counter == 0) {
                bootstrap.getTerminalService().outPutString(Constants.TASKS_DO_NOT_EXIST);
                return;
            }
            for (Task task : values) {
                if (task.getUserId().equals(bootstrap.getUser().getId())) {
                    bootstrap.getTerminalService().outPutTask(task);
                }
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
