package ru.shumov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.service.TerminalService;
import ru.shumov.tm.service.Md5Service;

import java.security.NoSuchAlgorithmException;

public class UserPasswordUpdateCommand extends AbstractCommand{
    private ServiceLocator bootstrap;
    private final Role role = Role.USER;
    private final String name = "password update";
    private final String description = "password update: Изменение пароля пользователя.";

    public UserPasswordUpdateCommand(Bootstrap bootstrapImpl) {
        this.bootstrap = bootstrapImpl;
    }

    public Role getRole() {
        return role;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void execute() throws NoSuchAlgorithmException {
        @Nullable var user = bootstrap.getUser();
        if(user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        bootstrap.getTerminalService().outPutString(Constants.ENTER_OLD_PASSWORD);
        @NotNull final var oldPassword = bootstrap.getTerminalService().scanner();
        @NotNull final var output = bootstrap.getMd5Service().md5(oldPassword);
        if(output.equals(user.getPassword())) {
            bootstrap.getTerminalService().outPutString(Constants.ENTER_NEW_PASSWORD);
            @NotNull final var newPassword = bootstrap.getTerminalService().scanner();
            @NotNull final var password = bootstrap.getMd5Service().md5(newPassword);
            user.setPassword(password);
            bootstrap.getUserService().update(user);
            bootstrap.getTerminalService().outPutString(Constants.PASSWORD_UPDATE_SUCCESSFUL);
        } else {
            bootstrap.getTerminalService().outPutString(Constants.INVALID_PASSWORD);
        }
    }
}
