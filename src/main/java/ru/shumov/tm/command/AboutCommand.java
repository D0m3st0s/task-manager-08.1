package ru.shumov.tm.command;

import com.jcabi.manifests.Manifests;


import java.security.NoSuchAlgorithmException;

public class AboutCommand extends AbstractCommand{

    private String name = "about";
    private String description = "about: Информация о сборке приложения.";
    public void execute() throws NoSuchAlgorithmException {
        System.out.println("[ABOUT]");
        System.out.println("Version of build: " + Manifests.read("BuildNumber"));
        System.out.println("Built by: " + Manifests.read("Built-By"));

    }
    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
