package ru.shumov.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.service.TerminalService;

public class UserLogOutCommand extends AbstractCommand {
    private ServiceLocator bootstrap;
    private final String name = "log out";
    private final String description = "log out: Выход из учётной записи";

    public UserLogOutCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void execute()  {
        @Nullable var user = bootstrap.getUser();
        if(user == null) {
            bootstrap.getTerminalService().outPutString(Constants.USER_DID_NOT_AUTHORIZED);
            return;
        }
        bootstrap.setUser(null);
        bootstrap.getTerminalService().outPutString(Constants.LOG_OUT_SUCCESSFUL);
    }
}
