package ru.shumov.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.service.CommandsServiceImpl;

import java.util.Collection;

public class Help extends AbstractCommand{
    private ServiceLocator bootstrap;
    private CommandsServiceImpl commands;
    private String name = "help";
    private String description = "help : Вывод доступных команд.";

    public Help(CommandsServiceImpl commands, Bootstrap bootstrap) {
        this.commands = commands;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        @NotNull Collection<AbstractCommand> value = commands.getList();
        for(AbstractCommand abstractCommand : value){
            bootstrap.getTerminalService().outPutString(abstractCommand.getDescription());
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
