package ru.shumov.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.service.TerminalService;

import java.security.NoSuchAlgorithmException;

public class UserShowCommand extends AbstractCommand {
    private ServiceLocator bootstrap;
    private final Role role = Role.USER;
    private final String name = "show";
    private final String description = "show: Вывод информации о пользователе.";

    public UserShowCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public Role getRole() {return role;}

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void execute() throws NoSuchAlgorithmException {
        @Nullable var user = bootstrap.getUser();
        if(user == null) {
            bootstrap.getTerminalService().outPutString(Constants.USER_DID_NOT_AUTHORIZED);
            return;
        }
        bootstrap.getTerminalService().outPutUser(user);
    }
}
