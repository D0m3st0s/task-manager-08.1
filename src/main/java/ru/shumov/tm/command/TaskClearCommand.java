package ru.shumov.tm.command;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.service.TerminalService;

public class TaskClearCommand extends AbstractCommand{
    private ServiceLocator bootstrap;
    private final Role role = Role.ADMINISTRATOR;
    private final String name = "task clear";
    private final String description = "task clear: Удаление всех задач.";

    public TaskClearCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        @Nullable var user = bootstrap.getUser();
        if(user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ALL_TASKS_WILL_BE_CLEARED);
            bootstrap.getTerminalService().outPutString(Constants.YES_NO);
            @NotNull final var answer = bootstrap.getTerminalService().scanner();
            if (Constants.YES.equals(answer)) {
                bootstrap.getTaskService().clear();
            }
        }  else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
