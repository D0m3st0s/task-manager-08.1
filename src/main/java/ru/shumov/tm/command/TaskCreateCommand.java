package ru.shumov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.service.TerminalService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class TaskCreateCommand extends AbstractCommand{
    private ServiceLocator bootstrap;
    private final Role role = Role.USER;
    private final String name = "task create";
    private final String description = "task create: Создание нового задания.";

    public TaskCreateCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        @Nullable var user = bootstrap.getUser();
        if(user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            var format = new SimpleDateFormat();
            format.applyPattern("dd.MM.yyyy");
            bootstrap.getTerminalService().outPutString(Constants.ENTER_PROJECT_ID_FOR_TASKS);
            @NotNull final var projectId = bootstrap.getTerminalService().scanner();
            try {
                if (bootstrap.getProjectService().checkKey(projectId)) {
                    bootstrap.getTerminalService().outPutString(Constants.ENTER_TASK_NAME);
                    @NotNull final var name = bootstrap.getTerminalService().scanner();
                    Task task = new Task();
                    @NotNull final var userId = bootstrap.getUser().getId();
                    bootstrap.getTerminalService().outPutString(Constants.ENTER_START_DATE_OF_TASK);
                    @NotNull final var startDateS = bootstrap.getTerminalService().scanner();
                    Date StartDateD = format.parse(startDateS);
                    bootstrap.getTerminalService().outPutString(Constants.ENTER_DEADLINE_OF_TASK);
                    @NotNull final var endDateS = bootstrap.getTerminalService().scanner();
                    Date endDateD = format.parse(endDateS);
                    @NotNull final var id = UUID.randomUUID().toString();
                    bootstrap.getTerminalService().outPutString(Constants.ENTER_DESCRIPTION_OF_TASK);
                    @NotNull final var description = bootstrap.getTerminalService().scanner();

                    task.setDescription(description);
                    task.setEndDate(endDateD);
                    task.setStartDate(StartDateD);
                    task.setProjectId(projectId);
                    task.setName(name);
                    task.setId(id);
                    task.setUserId(userId);

                    bootstrap.getTaskService().create(task);
                } else {
                    bootstrap.getTerminalService().outPutString(Constants.PROJECT_DOES_NOT_EXIST);
                }
            } catch (ParseException parseException) {
                bootstrap.getTerminalService().outPutString(Constants.INCORRECT_DATE_FORMAT);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
