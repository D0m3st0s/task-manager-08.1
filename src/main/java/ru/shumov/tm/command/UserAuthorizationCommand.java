package ru.shumov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.service.TerminalService;
import ru.shumov.tm.service.Md5Service;

import java.security.NoSuchAlgorithmException;

public class UserAuthorizationCommand extends AbstractCommand {
    private ServiceLocator bootstrap;
    private final String name = "log in";
    private final String description = "log in: Авторизация пользователя.";

    public UserAuthorizationCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void execute() throws NoSuchAlgorithmException {
        @Nullable var user = bootstrap.getUser();
        if(user != null) {
            bootstrap.getTerminalService().outPutString(Constants.USER_ALREADY_AUTHORIZED);
            return;
        }
        bootstrap.getTerminalService().outPutString(Constants.ENTER_NAME_OF_USER);
        @NotNull var login = bootstrap.getTerminalService().scanner();
         user = bootstrap.getUserService().getOne(login);
        if(user != null) {
            bootstrap.getTerminalService().outPutString(Constants.ENTER_PASSWORD);
            @NotNull final var password = bootstrap.getTerminalService().scanner();
            @NotNull final var output = bootstrap.getMd5Service().md5(password);
            if(output.equals(user.getPassword())) {
                bootstrap.setUser(user);
                bootstrap.getTerminalService().outPutString(Constants.AUTHORIZATION_SUCCESSFUL);
            } else {
                bootstrap.getTerminalService().outPutString(Constants.INVALID_PASSWORD);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.INVALID_LOGIN);
        }
    }
}
