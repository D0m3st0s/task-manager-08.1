package ru.shumov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.enums.Role;

import java.util.Collection;

public class ProjectRemoveCommand extends AbstractCommand{
    private ServiceLocator bootstrap;
    private final Role role = Role.USER;
    private final String name = "project remove";
    private final String description = "project remove: Выборочное удаление проектов.";

    public ProjectRemoveCommand(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        @Nullable var user = bootstrap.getUser();
        if(user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ENTER_PROJECT_ID_FOR_REMOVING);
            @NotNull var projectId = bootstrap.getTerminalService().scanner();
            @Nullable final var project = bootstrap.getProjectService().getOne(projectId);
            if(project == null) {
                bootstrap.getTerminalService().outPutString(Constants.PROJECT_DOES_NOT_EXIST);
                return;
            }
            if (project.getUserId().equals(user.getId())) {
                bootstrap.getProjectService().remove(projectId);
            } else {
                bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
                return;
            }
            @Nullable Collection<Task> values = bootstrap.getTaskService().getList();
            for (Task task : values) {
                if (task.getProjectId().equals(projectId) && task.getUserId().equals(user.getId())) {
                    values.remove(task);
                }
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
