package ru.shumov.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.enums.Role;


import java.util.Collection;

public class ProjectGetListCommand extends AbstractCommand{
    private ServiceLocator bootstrap;
    private final Role role = Role.USER;
    private final String name = "project list";
    private final String description = "project list: Вывод всех проектов.";

    public ProjectGetListCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        int counter = 0;
        @Nullable var user = bootstrap.getUser();
        if(user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            @Nullable Collection<Project> values = bootstrap.getProjectService().getList();
            if (values.isEmpty()) {
                bootstrap.getTerminalService().outPutString(Constants.PROJECTS_DO_NOT_EXIST);
                return;
            }
            for(Project project : values) {
                if (project.getUserId().equals(bootstrap.getUser().getId())) {
                    counter++;
                }
            }
            if(counter == 0) {
                bootstrap.getTerminalService().outPutString(Constants.PROJECTS_DO_NOT_EXIST);
                return;
            }
            for (Project project : values) {
                if (project.getUserId().equals(bootstrap.getUser().getId())) {
                    bootstrap.getTerminalService().outPutProject(project);
                }
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
