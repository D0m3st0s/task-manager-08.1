package ru.shumov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.service.TerminalService;

public class TaskGetOneCommand extends AbstractCommand{
    private ServiceLocator bootstrap;
    private final Role role = Role.USER;
    private final String name = "get task";
    private final String description = "get task: вывод конкретной задачи.";

    public TaskGetOneCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        @Nullable var user = bootstrap.getUser();
        if(user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ENTER_TASK_ID_FOR_SHOWING_TASKS);
            @NotNull final var taskId = bootstrap.getTerminalService().scanner();
            @NotNull final var task = bootstrap.getTaskService().getOne(taskId);
            if (task.getUserId().equals(user.getId())) {
                bootstrap.getTerminalService().outPutTask(task);
            } else {
                bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
