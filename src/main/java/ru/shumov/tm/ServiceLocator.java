package ru.shumov.tm;

import ru.shumov.tm.entity.User;
import ru.shumov.tm.service.*;

public interface ServiceLocator {

    ProjectService getProjectService();

    TaskService getTaskService();

    UserService getUserService();

    User getUser();

    Md5Service getMd5Service();

    TerminalService getTerminalService();

    void setUser(User user);
}
