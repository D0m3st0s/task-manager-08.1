package ru.shumov.tm.exceptions;

public class IncorrectInput extends Exception{

    private String massage;

    public IncorrectInput(String s) {
        this.massage = s;
    }

    public String getMassage(){
        return massage;
    }
}
